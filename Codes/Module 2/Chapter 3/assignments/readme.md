# **Checks Where a point lies according to triangle**

## **How to use it :**
> - Enter co-ordinates of three corner points of triangle 
> - Enter the co-ordinate of Point P for whcih we have to check    
> - Answer is shown below the check button 

### Triangle diagram is shown below
![Triangle Image](maxresdefault.jpg "Triangle diagram")

### Functions Added
> - Finds point is inside a triangle or not
> - Checks whether a point is not triangle
> - Checks whether input is number or not and gives error according to i

### Solving 

> 1. Area of the given triangle, i.e., area of the triangle ABC is calculated according to given points ,by using formula given below   
> ``Area A = [ x1(y2 – y3) + x2(y3 – y1) + x3(y1-y2)]/2`` 
> 1. Calculate area of the triangle PAB. We can use the same formula for this. Let this area be A1.
> 1. Calculate area of the triangle PBC. Let this area be A2.
> 1. Calculate area of the triangle PAC. Let this area be A3.
>1. If P lies inside the triangle, then A1 + A2 + A3 must be equal to A.
> 1. If A= A1+A2+A3 then we check one more condition that if any area from A1,A2,A3 are zero that means the point lies on triangle
> 1. If it's not then it's outside the circle

### Things required to run the file
> - An IDE(prefered VScode) (Not necessary)
> - Download the code from gitlab as zip format and unzip it
> -  Click on html file and open it with any browser