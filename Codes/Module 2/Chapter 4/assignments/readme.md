# **Prints the Multiplication table**

## This page prints multiplication of number added by user 

### Functions Added
> - Prints Table upto inputed number
> - Checks whether inputed value is number or not
> - One can replace a value in either times field or number field

### How to calculate using this web app
> - Enter the any number greater than 0
> - Press Calculate to get the multiplication table
> - If one wants to random find a multiplication e.g 9 * 12 then one can manually put the values in Number and Times Column

### Things required to run the file
> - An IDE(prefered VScode) (Not necessary)
> - Download the code from gitlab as zip format and unzip it
> -  Click on html file and open it with any browser
