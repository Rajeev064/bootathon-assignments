function mul_table() {
    var input = document.getElementById("t1");
    var num = +input.value;
    var count = 1;
    var table = document.getElementById("table_1");
    if (isNaN(num)) //for non number value
     {
        alert("String value not allowed or enter a valid number");
    }
    else {
        while (table.rows.length > 1) {
            table.deleteRow(1);
        }
        for (count = 1; count <= num; count++) {
            var row = table.insertRow();
            var cell = row.insertCell();
            var text = document.createElement("input");
            // Prints Inputed number n times where n is inputed number
            var text = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.onblur = numb;
            text.value = num.toString();
            text.id = "1-" + count;
            cell.appendChild(text);
            //Adds * sign in column
            var txt = "*";
            var cell = row.insertCell();
            var text = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = txt.toString();
            cell.appendChild(text);
            //For printing column for times ,it will print numbers from 1 to input number
            var cell = row.insertCell();
            var text = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.onblur = times;
            text.value = count.toString();
            text.id = "2-" + count;
            cell.appendChild(text);
            //Adds  = sign in column
            var txt = "=";
            var cell = row.insertCell();
            var text = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = txt.toString();
            cell.appendChild(text);
            //Adds Product Section
            var cell = row.insertCell();
            var text = document.createElement("input");
            text.type = "text";
            text.id = "3-" + count; //id for elements in product column
            text.style.textAlign = "center";
            text.value = (count * num).toString();
            cell.appendChild(text);
        }
    }
}
// This functions are additional function for updating rows in times,number field
//this function is for updating the number column 
function numb(event) {
    var c = event.target.value;
    var id = event.target.id; //finds the row updated
    var a = document.getElementById("2-" + id.split('-')[1]).value; //takes value from times column of same row 
    var r = document.getElementById("3-" + id.split('-')[1]); //takes id of product column of same row 
    r.value = (+c * +a).toString(); //updating the product column according to changed value
}
//this function is for updating the number column 
function times(event) {
    var c = event.target.value;
    var id = event.target.id; //finds the row updated
    var a = document.getElementById("1-" + id.split('-')[1]).value; //takes value from number column of same row 
    var r = document.getElementById("3-" + id.split('-')[1]); //takes id of product column of same row 
    r.value = (+c * +a).toString(); //updating the product column according to changed value
}
//# sourceMappingURL=app.js.map